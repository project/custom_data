<?php

/**
 * @file
 * Hooks specific to the Custom Data module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Define a string representation for the given custom data.
 *
 * In case the hook implementation returns an empty string, a fallback value
 * will be generated, or another module might generate the value.
 *
 * @param \Drupal\custom_data\CustomDataInterface $custom_data
 *   The custom data.
 * @param string $string
 *   The current value of the string representation.
 *
 * @return string
 *   The generated string representation.
 *
 * @see \Drupal\custom_data\CustomDataInterface::getStringRepresentation()
 */
function hook_custom_data_get_string_representation(\Drupal\custom_data\CustomDataInterface $custom_data, $string) {
  if ($custom_data->isNew()) {
    return 'NEW - ' . $custom_data->get('my_custom_field')->value;
  }
  return $custom_data->get('my_custom_field')->value;
}

/**
 * @} End of "addtogroup hooks".
 */
