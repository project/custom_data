# Custom Data

**Manage custom data as its own content entity type.**

https://www.drupal.org/project/custom_data

## 0. Contents

- 1. Introduction
- 2. Requirements
- 3. Installation
- 4. Usage
- 5. Maintainers
- 6. Support and contribute

## 1. Introduction

Custom data is a place where data that is outside of the data structures of your
system may be stored. For example, if you want to give your customers a place to
create their own data structures and data sets, whose configuration is not
part of your deployment pipeline, this module is for you.

## 2. Requirements

This module builds on top of contrib Entity API.
Therefore, the contrib Entity API (https://www.drupal.org/project/entity) is
required to be additionally installed besides Drupal core.

## 3. Installation

Install the module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

## 4. Usage

Once installed, you may start creating your first custom data type at
/admin/structure/custom-data/add.

You should also take a look at the permissions page at /admin/people/permissions
and make sure whether the configured permissions are properly set.

## 5. Maintainers

* Maximilian Haupt (mxh) - https://www.drupal.org/u/mxh

## 6. Support and contribute

To submit bug reports and feature suggestions, or to track changes visit:
https://www.drupal.org/project/issues/custom_data

You can also use this issue queue for contributing, either by submitting ideas,
or new features and mostly welcome - patches and tests.
