<?php

namespace Drupal\custom_data\Entity;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\custom_data\CustomDataInterface;
use Drupal\custom_data\CustomDataTypeInterface;
use Drupal\entity\Revision\RevisionableContentEntityBase;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines custom data items as content entities.
 *
 * @ContentEntityType(
 *   id = "custom_data",
 *   label = @Translation("Custom data"),
 *   label_collection = @Translation("Custom data"),
 *   label_singular = @Translation("custom data"),
 *   label_plural = @Translation("custom data items"),
 *   label_count = @PluralTranslation(
 *     singular = "@count custom data record",
 *     plural = "@count custom data items",
 *   ),
 *   bundle_label = @Translation("Custom data type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "\Drupal\custom_data\CustomDataListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\custom_data\Form\CustomDataForm",
 *       "edit" = "Drupal\custom_data\Form\CustomDataForm",
 *       "delete" = "Drupal\custom_data\Form\CustomDataDeleteForm",
 *       "default" = "Drupal\custom_data\Form\CustomDataForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *       "revision" = "Drupal\entity\Routing\RevisionRouteProvider",
 *       "delete-multiple" = "Drupal\entity\Routing\DeleteMultipleRouteProvider",
 *     },
 *     "access" = "Drupal\entity\UncacheableEntityAccessControlHandler",
 *     "query_access" = "Drupal\entity\QueryAccess\UncacheableQueryAccessHandler",
 *     "permission_provider" = "Drupal\entity\UncacheableEntityPermissionProvider",
 *     "views_data" = "Drupal\entity\EntityViewsData"
 *   },
 *   base_table = "custom_data",
 *   data_table = "custom_data_field_data",
 *   revision_table = "custom_data_revision",
 *   revision_data_table = "custom_data_revision_data",
 *   revisionable = TRUE,
 *   show_revision_ui = TRUE,
 *   translatable = TRUE,
 *   admin_permission = "administer custom_data",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "rev_id",
 *     "langcode" = "langcode",
 *     "bundle" = "type",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *     "published" = "status",
 *     "uid" = "uid",
 *     "owner" = "uid"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "rev_uid",
 *     "revision_created" = "rev_timestamp",
 *     "revision_log_message" = "rev_log"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/custom-data/add/{custom_data_type}",
 *     "add-page" = "/admin/content/custom-data/add",
 *     "canonical" = "/custom-data/{custom_data}",
 *     "collection" = "/admin/content/custom-data",
 *     "edit-form" = "/custom-data/{custom_data}/edit",
 *     "delete-form" = "/custom-data/{custom_data}/delete",
 *     "duplicate-form" = "/custom-data/{custom_data}/duplicate",
 *     "delete-multiple-form" = "/custom-data/delete",
 *     "revision" = "/custom-data/{custom_data}/revisions/{custom_data_revision}/view",
 *     "revision-revert-form" = "/custom-data/{custom_data}/revisions/{custom_data_revision}/revert",
 *     "version-history" = "/custom-data/{custom_data}/revisions"
 *   },
 *   bundle_entity_type = "custom_data_type",
 *   field_ui_base_route = "entity.custom_data_type.edit_form",
 *   common_reference_target = TRUE,
 *   permission_granularity = "bundle",
 *   token_type = "custom_data"
 * )
 */
class CustomData extends RevisionableContentEntityBase implements CustomDataInterface {

  use EntityOwnerTrait, EntityPublishedTrait, EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new custom data item is created, set the uid entity reference to
   * the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $time = \Drupal::time()->getCurrentTime();
    $values += [
      'created' => $time,
      'changed' => $time,
      'uid' => static::getDefaultEntityOwner(),
    ];
    if (!isset($values['status'])) {
      /** @var \Drupal\custom_data\CustomDataTypeInterface $data_type */
      if (isset($values['type']) && ($data_type = \Drupal::entityTypeManager()->getStorage('custom_data_type')->load($values['type']))) {
        $values['status'] = $data_type->getStatus();
      }
      else {
        $values['status'] = FALSE;
      }
    }
    if (isset($values['label']) && $values['label'] !== '') {
      // Disable the label pattern when a label is already there.
      $values['label_pattern'] = '';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the custom data
    // owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }

    $this->applyLabelPattern();
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->get('label')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLabel($label) {
    $this->set('label', $label);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasLinkTemplate($rel) {
    if ($rel === 'canonical') {
      /** @var \Drupal\custom_data\CustomDataTypeInterface $type */
      $type = \Drupal::entityTypeManager()->getStorage('custom_data_type')->load($this->bundle());
      if (!$type->hasCanonical()) {
        return FALSE;
      }
    }
    return parent::hasLinkTemplate($rel);
  }

  /**
   * {@inheritdoc}
   */
  public function toUrl($rel = 'canonical', array $options = []) {
    if (!$this->hasLinkTemplate($rel)) {
      $args = func_get_args();
      if (empty($args) || empty(reset($args))) {
        // For any caller that does not explicitly require a canonical url,
        // offer the edit form url.
        $rel = 'edit-form';
      }
    }
    return parent::toUrl($rel, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function toLink($text = NULL, $rel = 'canonical', array $options = []) {
    if (!$this->hasLinkTemplate($rel)) {
      $args = func_get_args();
      if (count($args) < 2) {
        // For any caller that does not explicitly require a canonical url,
        // offer the edit form url.
        $rel = 'edit-form';
      }
    }
    return parent::toLink($text, $rel, $options);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(FALSE)
      ->setTranslatable(TRUE)
      ->setRevisionable(TRUE)
      ->setDefaultValue('')
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the custom data was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the custom data was last edited.'));

    $fields['uid']
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Author'))
      ->setDescription(t('The user ID of the custom data author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status']
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getStringRepresentation(): string {
    $string = '';
    \Drupal::moduleHandler()->invokeAllWith('custom_data_get_string_representation', function (callable $hook, string $module) use (&$string) {
      $string = $hook($this, $string);
    });

    if (trim($string) === '') {
      $string = $this->generateFallbackStringRepresentation();
    }

    if (mb_strlen($string) > 255) {
      $string = Unicode::truncate($string, 255, TRUE, TRUE, 20);
    }

    return $string;
  }

  /**
   * Implements the magic __toString() method.
   *
   * When a string representation is explicitly needed, consider directly using
   * ::getStringRepresentation() instead.
   */
  public function __toString() {
    return $this->getStringRepresentation();
  }

  /**
   * {@inheritdoc}
   */
  public function applyLabelPattern(array $token_data = []): void {
    if (isset($this->label_pattern)) {
      $label_pattern = $this->hasField('label_pattern') ? $this->get('label_pattern')->getString() : $this->label_pattern;
    }
    elseif ($type_id = $this->bundle()) {
      /** @var \Drupal\custom_data\CustomDataTypeInterface $type */
      if ($type = \Drupal::entityTypeManager()->getStorage('custom_data_type')->load($type_id)) {
        $label_pattern = $type->getLabelPattern();
      }
    }
    if (!empty($label_pattern)) {
      $string = (string) \Drupal::token()->replace($label_pattern, $token_data + ['custom_data' => $this], [
        'langcode' => $this->language()->getId(),
        'clear' => TRUE,
      ]);
      if (mb_strlen($string) > 255) {
        $string = Unicode::truncate($string, 255, TRUE, TRUE, 20);
      }
      $this->label->value = $string;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getType(): CustomDataTypeInterface {
    return \Drupal::entityTypeManager()->getStorage('custom_data_type')->load($this->bundle());
  }

  /**
   * Fallback method for generating a string representation.
   *
   * @see ::getStringRepresentation()
   *
   * @return string
   *   The fallback value for the string representation.
   */
  protected function generateFallbackStringRepresentation() {
    $components = \Drupal::service('entity_display.repository')->getFormDisplay('custom_data', $this->bundle())->getComponents();

    // The label is available in the form, thus the user is supposed to enter
    // a value for it. For this case, use the label directly and return it.
    if (!empty($components['label'])) {
      return $this->label();
    }

    uasort($components, 'Drupal\Component\Utility\SortArray::sortByWeightElement');
    $values = [];

    foreach (array_keys($components) as $field_name) {
      // Components can be extra fields, check if the field really exists.
      if (!$this->hasField($field_name)) {
        continue;
      }
      $field_definition = $this->getFieldDefinition($field_name);

      // Only take care for accessible string fields.
      if (!($field_definition instanceof FieldConfigInterface) || $field_definition->getType() !== 'string' || !$this->get($field_name)->access('view')) {
        continue;
      }

      if ($this->get($field_name)->isEmpty()) {
        continue;
      }

      foreach ($this->get($field_name) as $field_item) {
        $values[] = $field_item->value;
      }

      // Stop after two value items were received.
      if (count($values) > 2) {
        return implode(' ', array_slice($values, 0, 2)) . '...';
      }
    }

    return implode(' ', $values);
  }

}
