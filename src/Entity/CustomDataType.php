<?php

namespace Drupal\custom_data\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\custom_data\CustomDataTypeInterface;

/**
 * Defines custom data types as configuration entities.
 *
 * @ConfigEntityType(
 *   id = "custom_data_type",
 *   label = @Translation("Custom data type"),
 *   label_collection = @Translation("Custom data types"),
 *   label_singular = @Translation("custom data type"),
 *   label_plural = @Translation("custom data types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count custom data type",
 *     plural = "@count custom data types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\custom_data\Form\CustomDataTypeForm",
 *       "edit" = "Drupal\custom_data\Form\CustomDataTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\custom_data\CustomDataTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer custom data types",
 *   bundle_of = "custom_data",
 *   config_prefix = "custom_data_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/custom-data/add",
 *     "edit-form" = "/admin/structure/custom-data/manage/{custom_data_type}",
 *     "delete-form" = "/admin/structure/custom-data/manage/{custom_data_type}/delete",
 *     "collection" = "/admin/structure/custom-data"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "uuid",
 *     "status",
 *     "new_revision",
 *     "label_pattern",
 *     "has_canonical"
 *   }
 * )
 */
class CustomDataType extends ConfigEntityBundleBase implements CustomDataTypeInterface {

  /**
   * The machine name of this custom data type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the custom data type.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of this custom data type.
   *
   * @var string
   */
  protected $description;

  /**
   * Whether custom data items should be automatically created.
   *
   * @var bool
   */
  protected $status = TRUE;

  /**
   * Whether a new revision should be created by default.
   *
   * @var bool
   */
  protected $new_revision = FALSE;

  /**
   * A pattern to use for creating the label of the custom data.
   *
   * @var string
   */
  protected string $label_pattern = '[custom_data:string_representation]';

  /**
   * Whether items of this type have a canonical URL.
   *
   * @var bool
   */
  protected $has_canonical = FALSE;

  /**
   * {@inheritdoc}
   */
  public function shouldCreateNewRevision() {
    return $this->new_revision;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus(): bool {
    return (bool) $this->status;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status): CustomDataTypeInterface {
    $this->status = (bool) $status;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    return $this->set('description', $description);
  }

  /**
   * {@inheritdoc}
   */
  public function getLabelPattern(): string {
    return $this->label_pattern;
  }

  /**
   * {@inheritdoc}
   */
  public function setLabelPattern($pattern): CustomDataTypeInterface {
    $this->label_pattern = $pattern;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasCanonical() {
    return $this->has_canonical;
  }

}
