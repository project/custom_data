<?php

namespace Drupal\custom_data;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;
use Drupal\Core\Entity\RevisionableEntityBundleInterface;

/**
 * Interface for an custom data type.
 */
interface CustomDataTypeInterface extends ConfigEntityInterface, RevisionableEntityBundleInterface, EntityDescriptionInterface {

  /**
   * Get the default status for custom data of this type.
   *
   * @return bool
   *   The default status value.
   */
  public function getStatus(): bool;

  /**
   * Set the default status value.
   *
   * @param bool $status
   *   The default status value.
   *
   * @return $this
   */
  public function setStatus($status): CustomDataTypeInterface;

  /**
   * Get the pattern to use for creating the label of the custom data.
   *
   * @return string
   *   The label pattern.
   */
  public function getLabelPattern(): string;

  /**
   * Set the pattern to use for creating the label of the custom data.
   *
   * @param string $pattern
   *   The label pattern to set.
   *
   * @return $this
   */
  public function setLabelPattern($pattern): CustomDataTypeInterface;

  /**
   * Whether items of this type have a canonical URL.
   *
   * @return bool
   *   Returns TRUE when they are accessible via URL, FALSE otherwise.
   */
  public function hasCanonical();

}
