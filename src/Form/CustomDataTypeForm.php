<?php

namespace Drupal\custom_data\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\language\Entity\ContentLanguageSettings;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for custom data type forms.
 */
class CustomDataTypeForm extends BundleEntityFormBase {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs the CustomDataTypeForm object.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager) {
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static(
      $container->get('entity_field.manager')
    );
    $instance->setEntityTypeManager($container->get('entity_type.manager'));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\custom_data\CustomDataTypeInterface $data_type */
    $data_type = $this->entity;
    if ($this->operation == 'add') {
      $form['#title'] = $this->t('Add custom data type');
    }
    else {
      $form['#title'] = $this->t(
        'Edit type of %label custom data',
        ['%label' => $data_type->label()]
      );
    }

    $form['label'] = [
      '#title' => $this->t('Type label'),
      '#type' => 'textfield',
      '#default_value' => $data_type->label(),
      '#description' => $this->t('The human-readable name of this custom data type.'),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $data_type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => ['Drupal\custom_data\Entity\CustomDataType', 'load'],
        'source' => ['label'],
      ],
      '#description' => $this->t('A unique machine-readable name for this custom data type. It must only contain lowercase letters, numbers, and underscores.'),
    ];

    $form['description'] = [
      '#title' => $this->t('Description'),
      '#type' => 'textarea',
      '#default_value' => $data_type->getDescription(),
      '#description' => $this->t('Describe this custom data type. The text will be displayed on the <em>Add custom data item</em> page.'),
    ];

    $form['label_pattern'] = [
      '#title' => $this->t('Pattern for automatic label generation'),
      '#type' => 'textfield',
      '#description' => $this->t('Instead of manually entering a label on each custom data item within a form, you can define a label pattern here for auto-generating a value for it. This pattern will be applied everytime a custom data item is being saved. Tokens are allowed, e.g. [custom_data:string_representation]. Leave empty to not use a name pattern for items of this type. If a label pattern is being used, you may optionally hide the label field in the <em>Manage form display</em> settings.'),
      '#default_value' => $data_type->getLabelPattern(),
      '#size' => 255,
      '#maxlength' => 255,
    ];

    if ($this->moduleHandler->moduleExists('token')) {
      $form['label_pattern_help'] = [
        '#type' => 'container',
        'token_link' => [
          '#theme' => 'token_tree_link',
          '#token_types' => ['custom_data'],
          '#dialog' => TRUE,
        ],
      ];
    }
    else {
      $form['label_pattern']['#description'] .= ' ' . $this->t('To get a list of available tokens, install the <a target="_blank" rel="noreferrer noopener" href=":drupal-token" target="blank">contrib Token</a> module.', [':drupal-token' => 'https://www.drupal.org/project/token']);
    }

    $form['has_canonical'] = [
      '#type' => 'checkbox',
      '#title' => t('Has canonical URL'),
      '#default_value' => $data_type->hasCanonical(),
      '#description' => t('Whether items of this type have a canonical URL for being viewed (accessible via /custom-data/{id}). When enabled, <a href=":permissions_url" target="_blank">permissions</a> need to be properly defined.', [
        ':permissions_url' => '/admin/people/permissions',
      ]),
      '#access' => \Drupal::currentUser()->hasPermission('administer permissions'),
    ];

    $form['additional_settings'] = [
      '#type' => 'vertical_tabs',
    ];

    $form['workflow'] = [
      '#type' => 'details',
      '#title' => $this->t('Publishing options'),
      '#group' => 'additional_settings',
    ];

    $form['workflow']['options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Default options'),
      '#default_value' => $this->getWorkflowOptions(),
      '#options' => [
        'status' => $this->t('Published'),
        'new_revision' => $this->t('Create new revision'),
      ],
    ];

    $form['workflow']['options']['status']['#description'] = $this->t('Custom data items will be automatically published when created.');
    $form['workflow']['options']['new_revision']['#description'] = $this->t('Automatically create new revisions. Users with the "Administer custom data" permission will be able to override this option.');

    if ($this->moduleHandler->moduleExists('language')) {
      $form['language'] = [
        '#type' => 'details',
        '#title' => $this->t('Language settings'),
        '#group' => 'additional_settings',
      ];

      $language_configuration = ContentLanguageSettings::loadByEntityTypeBundle('custom_data', $data_type->id());
      $form['language']['language_configuration'] = [
        '#type' => 'language_configuration',
        '#entity_information' => [
          'entity_type' => 'custom_data',
          'bundle' => $data_type->id(),
        ],
        '#default_value' => $language_configuration,
      ];

      $form['#submit'][] = 'language_configuration_element_submit';
    }

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save custom data type');
    $actions['delete']['#value'] = $this->t('Delete custom data type');
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\custom_data\CustomDataTypeInterface $data_type */
    $data_type = $this->entity;

    $data_type->set('id', trim($data_type->id()));
    $data_type->set('label', trim($data_type->label()));
    $data_type->set('status', (bool) $form_state->getValue(['options', 'status']));
    $data_type->set('new_revision', (bool) $form_state->getValue(['options', 'new_revision']));
    if (\Drupal::currentUser()->hasPermission('administer permissions')) {
      $data_type->set('has_canonical', (bool) $form_state->getValue('has_canonical'));
    }
    else {
      $data_type->set('has_canonical', $data_type->hasCanonical());
    }
    $status = $data_type->save();

    $t_args = ['%name' => $data_type->label()];
    if ($status == SAVED_UPDATED) {
      $message = $this->t('The custom data type %name has been updated.', $t_args);
    }
    elseif ($status == SAVED_NEW) {
      $message = $this->t('The custom data type %name has been added.', $t_args);
    }
    $this->messenger()->addStatus($message);

    // Update workflow options.
    $fields = $this->entityFieldManager->getFieldDefinitions('custom_data', $data_type->id());
    // @todo Make it possible to get default values without an entity.
    //   https://www.drupal.org/node/2318187
    $custom_data = $this->entityTypeManager->getStorage('custom_data')->create(['type' => $data_type->id()]);
    foreach (['status'] as $field_name) {
      $value = (bool) $form_state->getValue(['options', $field_name]);
      if ($custom_data->$field_name->value != $value) {
        $fields[$field_name]->getConfig($data_type->id())->setDefaultValue($value)->save();
      }
    }

    $this->entityFieldManager->clearCachedFieldDefinitions();

    $form_state->setRedirectUrl($data_type->toUrl('collection'));
  }

  /**
   * Prepares workflow options to be used in the 'checkboxes' form element.
   *
   * @return array
   *   Array of options ready to be used in #options.
   */
  protected function getWorkflowOptions() {
    /** @var \Drupal\custom_data\CustomDataTypeInterface $data_type */
    $data_type = $this->entity;
    $workflow_options = [
      'status' => $data_type->getStatus(),
      'new_revision' => $data_type->shouldCreateNewRevision(),
    ];
    // Prepare workflow options to be used for 'checkboxes' form element.
    $keys = array_keys(array_filter($workflow_options));
    return array_combine($keys, $keys);
  }

}
