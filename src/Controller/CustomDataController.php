<?php

namespace Drupal\custom_data\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\Controller\EntityViewController;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\custom_data\CustomDataInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class CustomDataController.
 */
class CustomDataController implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Provides a page to render a single custom data item.
   *
   * This wraps the default entity view controller in the way that a check is
   * performed, whether the according type is configured to have a canonical
   * URL. It passes the rendering to the default view controller if enabled
   * to do so, and otherwise throws an exception to show a 404 page instead.
   *
   * @param \Drupal\custom_data\CustomDataInterface $custom_data
   *   The entity to render.
   * @param string $view_mode
   *   (optional) The view mode that should be used to display the entity.
   *   Defaults to 'full'.
   *
   * @return array
   *   A render array as expected by
   *   \Drupal\Core\Render\RendererInterface::render().
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   When the custom type is not configured to have canonical URLs.
   */
  public function viewCanonical(CustomDataInterface $custom_data, $view_mode = 'full') {
    /** @var \Drupal\custom_data\CustomDataTypeInterface $data_type */
    $data_type = $this->entityTypeManager->getStorage('custom_data_type')->load($custom_data->bundle());
    if (!$data_type->hasCanonical()) {
      if ($custom_data->access('update')) {
        return $this->redirect('entity.custom_data.edit_form', ['custom_data' => $custom_data->id()], [], 302);
      }
      throw new NotFoundHttpException();
    }
    if (!$custom_data->access('view')) {
      throw new AccessDeniedHttpException();
    }
    return (new EntityViewController($this->entityTypeManager, $this->renderer))->view($custom_data, $view_mode);
  }

  /**
   * Custom access callback for ::viewCanonical().
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   * @param \Drupal\custom_data\CustomDataInterface $custom_data
   *   The requested entity.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function viewCanonicalAccess(AccountInterface $account, CustomDataInterface $custom_data) {
    // Local menu links are being built up using a "fake" route match. Therefore
    // we catch the current route match from the global container instead.
    $current_route_match = \Drupal::routeMatch();
    $route = $current_route_match->getRouteObject();

    if ($route && ($route->getDefault('_controller') === 'Drupal\custom_data\Controller\CustomDataController::viewCanonical')) {
      // Let ::viewCanonical finally decide whether access is allowed.
      return AccessResult::allowed()
        ->addCacheContexts(['url.path', 'url.query_args'])
        ->addCacheableDependency($custom_data);
    }

    /** @var \Drupal\custom_data\CustomDataTypeInterface $data_type */
    $data_type = $this->entityTypeManager->getStorage('custom_data_type')->load($custom_data->bundle());
    if (!$data_type->hasCanonical()) {
      return AccessResult::forbidden()
        ->addCacheContexts(['route.name'])
        ->addCacheTags(['config:custom_data.custom_data_type.' . $data_type->id()])
        ->addCacheableDependency($custom_data);
    }
    return $custom_data->access('view', $account, TRUE)
      ->addCacheTags(['config:custom_data.custom_data_type.' . $data_type->id()])
      ->addCacheableDependency($custom_data);
  }

  /**
   * Returns a redirect response object for the specified route.
   *
   * @param string $route_name
   *   The name of the route to which to redirect.
   * @param array $route_parameters
   *   (optional) Parameters for the route.
   * @param array $options
   *   (optional) An associative array of additional options.
   * @param int $status
   *   (optional) The HTTP redirect status code for the redirect. The default is
   *   302 Found.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  protected function redirect($route_name, array $route_parameters = [], array $options = [], $status = 302) {
    $options['absolute'] = TRUE;
    return new RedirectResponse(Url::fromRoute($route_name, $route_parameters, $options)->toString(), $status);
  }

}
