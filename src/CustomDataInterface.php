<?php

namespace Drupal\custom_data;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for custom data.
 */
interface CustomDataInterface extends ContentEntityInterface, EntityOwnerInterface, EntityPublishedInterface, EntityChangedInterface {

  /**
   * Gets the custom data label.
   *
   * @return string
   *   Label of the custom data.
   */
  public function getLabel();

  /**
   * Sets the custom data label.
   *
   * @param string $label
   *   The custom data label.
   *
   * @return \Drupal\custom_data\CustomDataInterface
   *   The called custom data entity.
   */
  public function setLabel($label);

  /**
   * Gets the custom data creation timestamp.
   *
   * @return int
   *   Creation timestamp of the custom data.
   */
  public function getCreatedTime();

  /**
   * Sets the custom data creation timestamp.
   *
   * @param int $timestamp
   *   The custom data creation timestamp.
   *
   * @return \Drupal\custom_data\CustomDataInterface
   *   The called custom data entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Get a brief string representation of this custom data.
   *
   * The returned string has a maximum length of 255 characters.
   * Warning: This might expose undesired field content.
   *
   * This method is not implemented as __toString(). Instead it is this method
   * name, to guarantee compatibility with future changes of the Entity API.
   * Another reason is, that this method is kind of a last resort for generating
   * the custom data label, and is not supposed to be used for other purposes
   * like serialization.
   *
   * Modules may implement hook_custom_data_get_string_representation() to
   * change the final result, which will be returned by this method.
   *
   * @return string
   *   The string representation of this custom data.
   */
  public function getStringRepresentation(): string;

  /**
   * Applies a label pattern to update the label property.
   *
   * Developers may define a custom label pattern by setting a public
   * "label_pattern" as string property or field. If it is not set, then the
   * configured label pattern in the corresponding type config will be used.
   *
   * @param array $token_data
   *   (optional) Data to use for Token replacement.
   */
  public function applyLabelPattern(array $token_data = []): void;

  /**
   * Get the according custom data type (i.e. the bundle as object).
   *
   * @return \Drupal\custom_data\CustomDataTypeInterface
   *   The custom data type as object.
   */
  public function getType(): CustomDataTypeInterface;

}
