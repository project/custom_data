<?php

/**
 * @file
 * Builds placeholder replacement tokens for "custom data"-related data.
 */

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function custom_data_token_info() {
  $type = [
    'name' => t('Custom data'),
    'description' => t('Tokens related to individual custom data items.'),
    'needs-data' => 'custom_data',
  ];

  $custom_data = [];

  // Core tokens for custom data items.
  $custom_data['id'] = [
    'name' => t("Custom data ID"),
    'description' => t('The unique ID of the custom data item.'),
  ];
  $custom_data['rev_id'] = [
    'name' => t("Revision ID"),
    'description' => t("The unique ID of the custom data's latest revision."),
  ];
  $custom_data['type'] = [
    'name' => t("Machine name of the custom data type"),
  ];
  $custom_data['type_label'] = [
    'name' => t("Human-readable label of the custom data type."),
    'description' => t("The human-readable name of the custom data type."),
  ];
  $custom_data['label'] = [
    'name' => t("Label"),
  ];
  $custom_data['langcode'] = [
    'name' => t('Language code'),
    'description' => t('The language code of the language the field bundle is written in.'),
  ];
  $custom_data['url'] = [
    'name' => t("URL"),
    'description' => t("The canonical URL of the custom data item."),
  ];
  $custom_data['edit-url'] = [
    'name' => t("Edit URL"),
    'description' => t("The URL of the custom data item's edit form."),
  ];

  // Chained tokens for custom data items.
  $custom_data['created'] = [
    'name' => t("Date created"),
    'type' => 'date',
  ];
  $custom_data['changed'] = [
    'name' => t("Date changed"),
    'type' => 'date',
  ];
  $custom_data['author'] = [
    'name' => t("Author"),
    'type' => 'user',
  ];

  // Miscellaneous tokens for custom data items.
  $custom_data['string_representation'] = [
    'name' => t("String representation"),
    'description' => t("A generic string representation of this custom data. Warning: This might expose undesired field content."),
  ];

  return [
    'types' => ['custom_data' => $type],
    'tokens' => ['custom_data' => $custom_data],
  ];
}

/**
 * Implements hook_tokens().
 */
function custom_data_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if ($type == 'custom_data' && !empty($data['custom_data'])) {
    $token_service = \Drupal::token();

    $url_options = ['absolute' => TRUE];
    if (isset($options['langcode'])) {
      $url_options['language'] = \Drupal::languageManager()->getLanguage($options['langcode']);
      $langcode = $options['langcode'];
    }
    else {
      $langcode = LanguageInterface::LANGCODE_DEFAULT;
    }

    /** @var \Drupal\custom_data\CustomDataInterface $custom_data */
    $custom_data = $data['custom_data'];
    if (isset($options['langcode']) && $custom_data->language()->getId() !== $options['langcode'] && $custom_data->hasTranslation($options['langcode'])) {
      $custom_data = $custom_data->getTranslation($options['langcode']);
    }
    $bubbleable_metadata->addCacheableDependency($custom_data);
    if ($type_id = $custom_data->bundle()) {
      if ($custom_data_type = \Drupal::entityTypeManager()->getStorage('custom_data_type')->load($type_id)) {
        $bubbleable_metadata->addCacheableDependency($custom_data_type);
      }
    }

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'id':
          $replacements[$original] = $custom_data->id();
          break;

        case 'rev_id':
          $replacements[$original] = $custom_data->getRevisionId();
          break;

        case 'type':
          $replacements[$original] = $type_id;
          break;

        case 'type_label':
          $replacements[$original] = !empty($custom_data_type) ? t($custom_data_type->label()) : '';
          break;

        case 'label':
          $replacements[$original] = $custom_data->label();
          break;

        case 'langcode':
          $replacements[$original] = $custom_data->language()->getId();
          break;

        case 'url':
          $replacements[$original] = $custom_data->toUrl('canonical', $url_options)->toString();
          break;

        case 'edit-url':
          $replacements[$original] = $custom_data->toUrl('edit-form', $url_options)->toString();
          break;

        // Default values for the chained tokens handled below.
        case 'created':
          $date_format = DateFormat::load('medium');
          $bubbleable_metadata->addCacheableDependency($date_format);
          $replacements[$original] = \Drupal::service('date.formatter')->format($custom_data->getCreatedTime(), 'medium', '', NULL, $langcode);
          break;

        case 'changed':
          $date_format = DateFormat::load('medium');
          $bubbleable_metadata->addCacheableDependency($date_format);
          $replacements[$original] = \Drupal::service('date.formatter')->format($custom_data->getChangedTime(), 'medium', '', NULL, $langcode);
          break;

        case 'author':
          $account = $custom_data->getOwner() ?: \Drupal::entityTypeManager()->getStorage('user')->load(0);
          $bubbleable_metadata->addCacheableDependency($account);
          $replacements[$original] = $account->label();
          break;

        case 'string_representation':
          $replacements[$original] = $custom_data->getStringRepresentation();
          break;
      }
    }

    if ($author_tokens = $token_service->findWithPrefix($tokens, 'author')) {
      $replacements += $token_service->generate('user', $author_tokens, ['user' => $custom_data->getOwner()], $options, $bubbleable_metadata);
    }

    if ($created_tokens = $token_service->findWithPrefix($tokens, 'created')) {
      $replacements += $token_service->generate('date', $created_tokens, ['date' => $custom_data->getCreatedTime()], $options, $bubbleable_metadata);
    }

    if ($changed_tokens = $token_service->findWithPrefix($tokens, 'changed')) {
      $replacements += $token_service->generate('date', $changed_tokens, ['date' => $custom_data->getChangedTime()], $options, $bubbleable_metadata);
    }
  }

  return $replacements;
}
